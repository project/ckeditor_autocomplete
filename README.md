INTRODUCTION
============
Adds in the Autocomplete plugin for CKEditor.

This is required by plugins that need autocomplete plugin to work,
like Emoji.

REQUIREMENTS
============
This module requires the core CKEDITOR module.

This module requires the following modules:
 * CKEditor Textwatcher (https://www.drupal.org/project/textwatcher)


CONFIGURATION
============
Other plugins can leverage autocomplete by adding this in their plugin
definition:

```
class YourButtonPlugin extends CKEditorPluginBase 
implements CKEditorPluginConfigurableInterface {
  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return ['autocomplete'];
  }

  // more code...
```

INSTALLATION
============

This module requires the core CKEditor module.

1.Download the library from https://ckeditor.com/cke4/addon/autocomplete

2.Place the library in the root libraries folder (/libraries).

3.Enable CKEditor Autocomplete module.

LIBRARY INSTALLATION (COMPOSER)
-------------------------------
1.Copy the following into your project's composer.json file.

Autocomplete
```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/autocomplete",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/autocomplete/releases/autocomplete_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

Xml
```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/textwatcher",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/textwatcher/releases/textwatcher_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```
2.Ensure you have following mapping inside your composer.json.
```
"extra": {
  "installer-paths": {
    "libraries/{$name}": ["type:drupal-library"]
  }
}
```
3.Run following commands to download required libraries.
```
composer require ckeditor-plugin/textwatcher
composer require ckeditor-plugin/autocomplete
```
4.Enable the CKEditor Autocomplete module.
